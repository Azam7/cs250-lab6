•	
•	#include "stdafx.h"
•	#include <iostream>
•	#include <vector>
•	#include <stdio.h>      
•	#include <stdlib.h>     
•	#include <time.h>  
•	#include <algorithm>
•	
•	using namespace std;
•	
•	bool moveMin(vector<int> &in);
•	bool testMoveMin(vector<int> &in, vector<int> &out);
•	int main(){
•	
•	
•		vector<int> in;
•		vector<int> out;
•		srand(time(0));
•		for (int i = 0; i < 30; i++){
•			int random_number = rand() % 100 + 1;
•			in.push_back(random_number);
•			out.push_back(random_number);
•		}
•	
•		//Prints out Unsorrted Array
•		cout << "Unsorted array is " << endl;
•		for (int i = 0; i < 30; i++){
•			cout << in[i] << " ";
•		}
•		cout << endl;
•		cout << endl;
•	
•		//Testing move min
•		testMoveMin(in,out);
•	
•		//Printing sorted array/
•		for (int i = 0; i < 30; i++){
•			cout << in[i] << " ";
•		}
•		cout << "\n" << endl;
•	
•	
•		system("pause");
•		return 0;
•	}
•	
•	
•	//moveMin function
•	bool moveMin(vector<int> &in){
•		for (int i = 0; i < in.size() - 2; i++){
•			for (int j = i + 1; j < in.size() - 1; j++){
•				if (in[i] > in[j]){
•					int temp = in[j];
•					in[j] = in[i];
•					in[i] = temp;
•				}
•	
•			}
•	
•	
•		}
•		return true;
•	
•	}
•	
•	
•	bool testMoveMin(vector<int> &in,vector<int> &out){
•	
•		//Calling function and measuring runtime
•		clock_t start;
•		start = clock();
•		moveMin(in);
•		cout << "Running time of moveMin is " << double(clock() - start / CLOCKS_PER_SEC) << endl;
•		cout << endl;
•	
•	
•		//Calling built-in sort function and measuring runtime
•		start = clock();
•		sort(out.begin(), out.end());
•		cout << "Running time of sort is " << double(clock() - start / CLOCKS_PER_SEC) << endl;
•		cout << endl;
•	
•	
•		return true;
•	
