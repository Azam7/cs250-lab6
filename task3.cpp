include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>

using namespace std;

bool moveMin(vector<int> &in);
bool testMoveMin(vector<int> &in, vector<int> &out);
int main(){


	vector<int> in;
	vector<int> out;
	srand(time(0));
	for (int i = 0; i < 30; i++){
		int random_number = rand() % 100 + 1;
		in.push_back(random_number);
		out.push_back(random_number);
	}

	//Prints out Unsorrted Array
	cout << "Unsorted array is " << endl;
	for (int i = 0; i < 30; i++){
		cout << in[i] << " ";
	}
	cout << endl;
	cout << endl;

	//Testing move min
	testMoveMin(in,out);

	//Printing sorted array/
	for (int i = 0; i < 30; i++){
		cout << in[i] << " ";
	}
	cout << "\n" << endl;


	system("pause");
	return 0;
}


//moveMin function
bool moveMin(vector<int> &in){
	bool valuesSwitched = false;
    int len = in.size();

for(int i = 1; i <= len; i++)
{
    if(i == len)
    {
        if(!valuesSwitched) break;

        valuesSwitched = false;
        i = 1;
    }
    if(in[i - 1] > in[i])
    {
        int temp = in[i - 1];
        in[i - 1] = in[i];
        in[i] = temp;
        valuesSwitched = true;
    }
}

	return true;

}


bool testMoveMin(vector<int> &in,vector<int> &out){

	//Calling function and measuring runtime
	clock_t start;
	start = clock();
	moveMin(in);
	cout << "Running time of moveMin is " << double(clock() - start / CLOCKS_PER_SEC) << endl;
	cout << endl;


	//Calling built-in sort function and measuring runtime
	start = clock();
	sort(out.begin(), out.end());
	cout << "Running time of sort is " << double(clock() - start / CLOCKS_PER_SEC) << endl;
	cout << endl;


	return true;

}
